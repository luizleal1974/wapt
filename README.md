<div align="justify">

# WAPT

<i>Web Application for Proficiency Testing Provider</i> (<b>WAPT</b>) is a friendly interactive web application for assess the performance of interlaboratory comparison. This software was developed by <a target='_blank' rel='noopener noreferrer' href='http://lattes.cnpq.br/8849370590932263' title='Shiny/R Developer'>Luiz Henrique Leal</a> (Shiny/R developer) and <a target='_blank' rel='noopener noreferrer' href='http://lattes.cnpq.br/1030306192969513' title='Advisor'>Werickson Fortunato de Carvalho Rocha</a> (advisor). The software was partial fulfilment of the requirements for the Degree of Doctor of Science in Metrology. It is available freely under the developer’s <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/wapt/blob/main/Application/DISCLAIMER.md'>`license`</a>. For any comments or questions, please send an e-mail to statistical.metrology@gmail.com.

WAPT provides statistical procedures for assess the performance of interlaboratory comparison according to type of data: (i) Univariate proficiency testing deal with measures (true replicates) from one single variable; (ii) Categorical proficiency testing when reported results are a classification about item test; (iii) Multivariate proficiency testing cope with multiple measurements on each participating. Furthermore, there are statistical tools for homogeneity and stability studies.

This application was developed using Shiny/R. <a target='_blank' rel='noopener noreferrer' href='https://shiny.rstudio.com/'>Shiny</a> is an open-source R package that provides a web framework for building interactive web applications using R (without requiring HTML, CSS, or JavaScript knowledge). <a target='_blank' rel='noopener noreferrer' href='https://www.r-project.org/'>R</a> is a free software environment for statistical computing and graphics. Finally, the software is available on web site https://statisticalmetrology.shinyapps.io/waptprovider/. In cases where there is difficulty in accessing the internet, this application can be downloaded, installed, and run locally according to the instructions contained in this repository.

<p> <br /> <p>

# Install

The user must download and unzip the <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/wapt/blob/main/Application/R.zip'>`R.zip`</a> file into <b>`C`</b> folder.

<p> <br /> <p>

# Requirements

Operating system: Microsoft &#174; Windows &#174; 10 or higher.

Storage space: a minimum of 1GB.

<a target='_blank' rel='noopener noreferrer' href='https://www.java.com'>Java</a> installed and updated.

<p> <br /> <p>

# Run

The user needs to double click on the <b>`Rgui.exe`</b> file available in the folder <b>`C:\R\R-4.1.1\bin\i386`</b> for 32 bits PC or <b>`C:\R\R-4.1.1\bin\x64`</b> for 64 bits PC (it is suggested to create a <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/wapt/blob/main/Application/Shortcut.pdf'>`desktop shortcut`</a> for this file). <i>Rgui.exe</i> runs as a standard Windows GUI executable and provides an R console in its own window. Click on `Application/Run/WAPTprovider` in R console menu to run the application. The software will open in the default web browser. If it is necessary to interrupt the execution of the application, close the web browser, return to the R console and click on the `stop` icon.


<p> <br /> <p>

# References

[1] LEAL, Luiz Henrique da Conceição. <b>Aplicativo web para avaliação de desempenho de laboratórios.</b> 2022. 146f. Tese (Doutorado em Metrologia) – Instituto Nacional de Metrologia, Qualidade e Tecnologia, Duque de Caxias, RJ, 2022. <a target='_blank' rel='noopener noreferrer' href='https://sucupira-legado.capes.gov.br/sucupira/public/consultas/coleta/trabalhoConclusao/viewTrabalhoConclusao.jsf?popup=true&id_trabalho=11606069'><img src="https://img.shields.io/badge/L435a-CDD.006.76-green.svg" alt="drawing"/></a>

[2] da Conceição Leal, L.H., Violante, F.G.M., de Carvalho, L.J. <i>et al</i>. A new methodology for proficiency testing scheme interpretation based on residual analysis. <i>Accred Qual Assur</i> <b>27</b>, 349–357 (2022). <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1007/s00769-022-01522-x'><img src="https://img.shields.io/badge/doi-10.1007/s00769.022.01522.x-yellow.svg" alt="drawing"/></a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://link.springer.com/journal/769/volumes-and-issues/27-6'>`(Volume 27, Issue 6, December 2022)`</a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://rdcu.be/dI7pC'>&#128065;</a>

[3] Leal LHC, Rocha WFC. A New Approach for Multivariate Data Analysis in Interlaboratory Comparisons Based on Multidimensional Scaling and Robust Confidence Ellipse. <i>J. Braz. Chem. Soc</i>. 2023;34(3):434-440. <a target='_blank' rel='noopener noreferrer' href='https://dx.doi.org/10.21577/0103-5053.20220121'><img src="https://img.shields.io/badge/doi-10.21577/0103.5053.20220121-yellow.svg" alt="drawing"/></a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://jbcs.sbq.org.br/default.asp?ed=336'>`(Volume 34, Number 3, March 2023)`</a>

[4] da Conceição Leal, L.H., Alves, M.L. & de Carvalho Rocha, W.F. Proficiency testing interpretation based on analysis of variance method. <i>Accred Qual Assur</i> <b>28</b>, 183–186 (2023). <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1007/s00769-023-01548-9'><img src="https://img.shields.io/badge/doi-10.1007/s00769.023.01548.9-yellow.svg" alt="drawing"/></a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://link.springer.com/journal/769/volumes-and-issues/28-4'>`(Volume 28, Issue 4, August 2023)`</a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://rdcu.be/dI7pk'>&#128065;</a>

[5] Leal, L.H.d.C., Rocha, W.F.d.C. Model Adequacy Checking in Homogeneity and Stability Studies. <i>MAPAN</i> <b>39</b>, 445–448 (2024).<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1007/s12647-023-00695-1'><img src="https://img.shields.io/badge/doi-10.1007/s12647.023.00695.1-yellow.svg" alt="drawing"/></a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://link.springer.com/journal/12647/volumes-and-issues/39-2'>`(Volume 39, Issue 2, June 2024)`</a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://rdcu.be/dI7jK'>&#128065;</a>


<p> <br /> <p>

</div>
