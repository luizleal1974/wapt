# License

<div align="justify">

<p>This software is provided by the developers as a public service and is expressly provided <i>"as is"</i>. The developers make no warranties, express, implied or statutory. This includes the implied warranty of merchantability, fitness for a particular purpose, non-infringement and data accuracy. The developers do not warrant or represent the use of the software or the results thereof. This includes but is not limited to software correctness, accuracy, reliability or usefulness. The developers shall not be liable and the software user hereby releases the developers from liability for any indirect, consequential, special, or incidental damages (such as damages for loss of business profits, business interruption, loss of business information, and the like), whether arising in tort, contract, or otherwise, arising from or relating to the software (or the use of or inability to use this software), even if the developers have been advised of the possibility of such damages.</p>

<p>The user is solely responsible for determining the appropriateness of using the software and assumes all risks associated with its use, including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and the unavailability or interruption of operation. This software is not intended to be used in any situation where failure could cause injury or property damage.</p>

<p>Permission to use this software is contingent upon this agreement. In addition, the user must acknowledge the developer's contribution to the software. Please explicitly acknowledge both developers as the software source. For information, please send an e-mail to <a href="mailto:statistical.metrology@gmail.com">statistical.metrology@gmail.com</a>.</p>

</div>
